import React from 'react';
import {Provider} from 'react-redux';
import configureStore from './redux/Store';

import GameScreen from './screens/GameScreen';

const App = () => {
  /** creating Redux store variable*/
  const store = configureStore();

  return (
    <Provider store={store}>
      <GameScreen />
    </Provider>
  );
};

export default App;
