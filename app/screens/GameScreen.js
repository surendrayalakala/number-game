import React, {useEffect, useRef} from 'react';
import {StyleSheet, View, ScrollView, Alert, Animated} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import Card from '../components/Card';
import Header from '../components/Header';
import allActions from '../redux/actions';
import Colors from '../utils/Colors';
import Labels from '../utils/Labels';
import * as methods from '../utils/Methods';

function GameScreen() {
  const gameReducer = useSelector(state => state.gameReducer);
  const dispatch = useDispatch();

  const timeout = useRef(null);

  useEffect(() => {
    dispatch(
      allActions.gameActions.suffleCards(
        methods.generateUniqueNums(Labels.ARRAY_SIZE, Labels.NUM_LIMIT),
      ),
    );
  }, []);

  useEffect(() => {
    let timeout = null;

    if (gameReducer.mappedNumbers.length === 2) {
      timeout = setTimeout(evaluate, 300);
    }
    return () => {
      clearTimeout(timeout);
    };
  }, [gameReducer.mappedNumbers]);

  useEffect(() => {
    if (
      Object.keys(gameReducer.completedNumbers).length > 0 &&
      Object.keys(gameReducer.completedNumbers).length ===
        gameReducer.randomNumbersArray.length
    ) {
      checkCompletion();
    }
  }, [gameReducer.completedNumbers]);

  /**
   * @function checkCompletion
   * @description will check whether game over or not
   */
  function checkCompletion() {
    if (
      Object.keys(gameReducer.completedNumbers).length ===
      gameReducer.randomNumbersArray.length
    ) {
      Alert.alert(
        Labels.CONGRATS,
        `You win this game by ${gameReducer.steps} steps!`,
        [
          {
            text: Labels.TRY_AGAIN,
            onPress: () => handleRestart(),
            style: 'ok',
          },
        ],
      );
    }
  }

  /**
   * @function evaluate
   * @description Check if both the cards have same number. If they do, mark them as inactive
   */
  function evaluate() {
    const [first, second] = gameReducer.mappedNumbers;
    if (
      gameReducer.shuffledCards[first] === gameReducer.shuffledCards[second]
    ) {
      const completedNumbers = {
        ...gameReducer.completedNumbers,
        [gameReducer.shuffledCards[first]]: true,
      };

      dispatch(
        allActions.gameActions.evaluteMappedNumbers(completedNumbers, []),
      );
    } else {
      timeout.current = setTimeout(() => {
        dispatch(allActions.gameActions.resetMappedNumbers([]));
      }, 1000);
    }
  }

  /**
   * @function handleCardClick
   * @param {*} index of clicked card number in array
   * @description this can call whenever clicked on card
   */
  function handleCardClick(index) {
    if (gameReducer.mappedNumbers.length === 1) {
      const updatedMappedNumbers = [...gameReducer.mappedNumbers, index];
      dispatch(
        allActions.gameActions.handleCardClick(
          updatedMappedNumbers,
          gameReducer.steps + 1,
        ),
      );
    } else {
      clearTimeout(timeout.current);
      dispatch(
        allActions.gameActions.handleCardClick([index], gameReducer.steps + 1),
      );
    }
  }

  /**
   * @function checkIsFlipped
   * @param {*} index of each number
   * @returns boolean value of specific index number flipped or not
   */
  function checkIsFlipped(index) {
    if (gameReducer.mappedNumbers.includes(index)) {
      return true;
    } else {
      const clearedCardsArray = Object.keys(gameReducer.completedNumbers).map(
        item => Number(item),
      );

      if (clearedCardsArray.length > 0) {
        if (clearedCardsArray.includes(gameReducer.shuffledCards[index])) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
  }

  const checkIsInactive = card => Boolean(gameReducer.completedNumbers[card]);

  /**
   * @function handleRestart
   * @description this can reset the app state
   */
  function handleRestart() {
    dispatch(
      allActions.gameActions.resetGame(
        methods.generateUniqueNums(Labels.ARRAY_SIZE, Labels.NUM_LIMIT),
      ),
    );
  }

  return (
    <View style={localStyles.container}>
      <Header onResetHandler={handleRestart} steps={gameReducer.steps} />
      <ScrollView style={localStyles.scrollContainer}>
        <View style={localStyles.cardsContainer}>
          {gameReducer.shuffledCards.map((cardNumber, index) => (
            <Card
              key={index}
              cardNumber={cardNumber}
              index={index}
              isInactive={checkIsInactive(cardNumber)}
              isFlipped={checkIsFlipped(index)}
              onClick={handleCardClick}
            />
          ))}
        </View>
      </ScrollView>
    </View>
  );
}

const localStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BLACK,
  },
  scrollContainer: {
    flex: 1,
  },
  cardsContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default GameScreen;
