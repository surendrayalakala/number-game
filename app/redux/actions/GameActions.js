/**
 * This file contains all game actions
 */

import * as actionTypes from '../ActionTypes';

/** Reset game  */
const resetGame = uniqueNumArray => ({
  type: actionTypes.RESET_GAME,
  payload: {
    uniqueNumArray,
  },
});

/** Reset mapped numbers */
const resetMappedNumbers = mappedNumbersArray => ({
  type: actionTypes.RESET_MAPPED_NUMBERS,
  payload: {
    mappedNumbersArray,
  },
});

/** Called for Evalute mapped numbers */
const evaluteMappedNumbers = (completedNumbers, mappedNumbersArray) => ({
  type: actionTypes.EVALUTE_MAPPED_NUMBERS,
  payload: {
    completedNumbers,
    mappedNumbersArray,
  },
});

/** Will call when clecked on card */
const handleCardClick = (updatedMappedNumbers, steps) => ({
  type: actionTypes.HANDLE_CARD_CLICK,
  payload: {
    updatedMappedNumbers,
    steps,
  },
});

/** Will call whenever user wants to suffle cards or numbers */
const suffleCards = uniqueNumArray => ({
  type: actionTypes.SUFFLE_CARDS,
  payload: {
    uniqueNumArray,
  },
});

export default {
  resetGame,
  resetMappedNumbers,
  evaluteMappedNumbers,
  handleCardClick,
  suffleCards,
};
