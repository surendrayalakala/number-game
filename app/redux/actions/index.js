/**
 * common file for all actions of redux
 */

import gameActions from './GameActions';

const allActions = {
  gameActions,
};

export default allActions;
