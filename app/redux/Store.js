/**
 * This file is related to redux store configuration
 */

import {createStore} from 'redux';

import rootReducer from './reducers/RootReducer';

/**  configuring the redux store */
const configureStore = () => createStore(rootReducer);

export default configureStore;
