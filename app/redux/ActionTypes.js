/**
 * This file contains all redux action type constants
 */

export const RESET_GAME = 'RESET_GAME';
export const RESET_MAPPED_NUMBERS = 'RESET_MAPPED_NUMBERS';
export const EVALUTE_MAPPED_NUMBERS = 'EVALUTE_MAPPED_NUMBERS';
export const HANDLE_CARD_CLICK = 'HANDLE_CARD_CLICK';
export const SUFFLE_CARDS = 'SUFFLE_CARDS';
