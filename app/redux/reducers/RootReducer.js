/**
 * In this file configuring the all reducers
 */

import {combineReducers} from 'redux';
import gameReducer from './GameReducer';

const rootReducer = combineReducers({
  gameReducer,
});

export default rootReducer;
