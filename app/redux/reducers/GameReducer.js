/**
 * Game reducer
 */

import * as methods from '../../utils/Methods';
import * as actionTypes from '../ActionTypes';

/** setting Initial state of game reducer */
const initialState = {
  randomNumbersArray: [],
  shuffledCards: [],
  mappedNumbers: [],
  completedNumbers: {},
  steps: 0,
};

const gameReducer = (state = initialState, actions) => {
  switch (actions.type) {
    /** Reset the game  */
    case actionTypes.RESET_GAME: {
      return {
        ...state,
        randomNumbersArray: actions.payload.uniqueNumArray,
        shuffledCards: methods.shuffleNumbers([
          ...actions.payload.uniqueNumArray,
          ...actions.payload.uniqueNumArray,
        ]),
        mappedNumbers: [],
        completedNumbers: {},
        steps: 0,
      };
    }

    /** Reset mapped numbers (here 2) */
    case actionTypes.RESET_MAPPED_NUMBERS: {
      return {
        ...state,
        mappedNumbers: actions.payload.mappedNumbersArray,
      };
    }

    /** Evalute mapped numbers in array */
    case actionTypes.EVALUTE_MAPPED_NUMBERS: {
      return {
        ...state,
        mappedNumbers: actions.payload.mappedNumbersArray,
        completedNumbers: actions.payload.completedNumbers,
      };
    }

    /** handle each click on card */
    case actionTypes.HANDLE_CARD_CLICK: {
      return {
        ...state,
        mappedNumbers: actions.payload.updatedMappedNumbers,
        steps: actions.payload.steps,
      };
    }

    /** suffle the numbers on cards */
    case actionTypes.SUFFLE_CARDS: {
      return {
        ...state,
        randomNumbersArray: actions.payload.uniqueNumArray,
        shuffledCards: methods.shuffleNumbers([
          ...actions.payload.uniqueNumArray,
          ...actions.payload.uniqueNumArray,
        ]),
      };
    }

    default:
      return state;
  }
};

export default gameReducer;
