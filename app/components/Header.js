/**
 * Header file of Game screen with Reset and steps count
 */

import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {hp, wp} from '../utils/ResponsiveUI';
import ColorCodes from '../utils/Colors';
import Labels from '../utils/Labels';

const Header = ({onResetHandler, steps}) => {
  return (
    <View style={localStyles.container}>
      <TouchableOpacity
        style={localStyles.resetButtonContainer}
        onPress={onResetHandler}>
        <Text style={localStyles.resetButtonLabel}>{Labels.RESET}</Text>
      </TouchableOpacity>
      <View style={localStyles.rightContainer}>
        <Text style={localStyles.stepsLabel}>{Labels.STEPS}</Text>
        <Text style={localStyles.stepsCount}>{steps}</Text>
      </View>
    </View>
  );
};

const localStyles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: ColorCodes.BLACK,
    height: hp(7),
    paddingHorizontal: wp(4),
    paddingVertical: hp(2),
  },
  stepsLabel: {
    color: ColorCodes.WHITE,
    fontSize: hp(1.8),
  },
  stepsCount: {
    color: ColorCodes.CYAN,
    fontSize: hp(2.5),
  },
  resetButtonContainer: {
    backgroundColor: ColorCodes.BLACK,
  },
  resetButtonLabel: {
    fontSize: hp(2.5),
    color: ColorCodes.CYAN,
    fontWeight: 'bold',
  },
  rightContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default Header;
