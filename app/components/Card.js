/**
 * Card component
 */

import React, {useRef} from 'react';
import {Text, StyleSheet, Animated, TouchableHighlight} from 'react-native';

import Colors from '../utils/Colors';
import {hp, wp} from '../utils/ResponsiveUI';

const Card = ({onClick, cardNumber, index, isInactive, isFlipped}) => {
  const flipAnimation = useRef(new Animated.Value(0)).current;

  let flipRotation = 0;
  flipAnimation.addListener(({value}) => {
    flipRotation = value;
  });

  const flipToFrontStyle = {
    transform: [
      {
        rotateY: flipAnimation.interpolate({
          inputRange: [0, 180],
          outputRange: ['0deg', '0deg'],
        }),
      },
    ],
  };

  const flipToBackStyle = {
    transform: [
      {
        rotateY: flipAnimation.interpolate({
          inputRange: [0, 180],
          outputRange: ['180deg', '360deg'],
        }),
      },
    ],
  };

  const flipCard = () => {
    if (flipRotation >= 90) {
      Animated.timing(flipAnimation, {
        toValue: 0,
        duration: 300,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(flipAnimation, {
        toValue: 180,
        duration: 300,
        useNativeDriver: true,
      }).start();
    }
  };

  const handleClick = () => {
    !isFlipped && !isInactive && flipCard();
    !isFlipped && !isInactive && onClick(index);
  };

  return (
    <TouchableHighlight
      key={index}
      style={localStyles.container}
      onPress={handleClick}>
      {!isFlipped ? (
        <Animated.View
          style={[
            localStyles.cardLabelStyle,
            localStyles.borderStyle,
            // {position: 'absolute', top: 0},
            flipToFrontStyle,
          ]}>
          <Text style={[localStyles.cardLabelStyle, localStyles.cardFontLabel]}>
            {'?'}
          </Text>
        </Animated.View>
      ) : (
        <Animated.View
          style={[
            localStyles.cardLabelStyle,
            localStyles.borderStyle,
            flipToBackStyle,
            // {backfaceVisibility: 'hidden'},
          ]}>
          <Text style={[localStyles.cardLabelStyle, localStyles.cardBackLabel]}>
            {cardNumber}
          </Text>
        </Animated.View>
      )}
    </TouchableHighlight>
  );
};

const localStyles = StyleSheet.create({
  container: {
    height: hp(100 / 5) - hp(0.5) * (5 + 1),
    width: wp(100 / 3) - wp(0.8) * (3 + 1),
    marginVertical: hp(0.5),
    marginHorizontal: wp(0.8),
  },
  cardLabelStyle: {
    fontSize: hp(3.5),
    height: '100%',
    width: '100%',
    textAlign: 'center',
    textAlignVertical: 'center',
    overflow: 'hidden',
  },
  cardFontLabel: {
    color: Colors.WHITE,
    backgroundColor: Colors.LIGHT_BLUE,
  },
  cardBackLabel: {
    color: Colors.BLACK,
    backgroundColor: Colors.WHITE,
  },
  borderStyle: {
    borderWidth: wp(0.5),
    borderRadius: hp(1.5),
    borderColor: Colors.WHITE,
  },
});

export default Card;
