/**
 * this file contains all common methods
 */

/**
 * @function generateUniqueNums
 * @param {*} arraySize
 * @param {*} limit
 * @returns unique numbers array
 */
export function generateUniqueNums(arraySize, limit) {
  const randomNums = new Set();
  while (randomNums.size !== arraySize) {
    randomNums.add(Math.floor(Math.random() * limit) + 1);
  }

  return [...randomNums];
}

/**
 * @function shuffleNumbers
 * @param {*} uniqueNumArray
 * @returns returns shufflled numbers array
 */
export function shuffleNumbers(uniqueNumArray) {
  const length = uniqueNumArray.length;
  for (let i = length; i > 0; i--) {
    const randomIndex = Math.floor(Math.random() * i);
    const currentIndex = i - 1;
    const currentNum = uniqueNumArray[currentIndex];
    uniqueNumArray[currentIndex] = uniqueNumArray[randomIndex];
    uniqueNumArray[randomIndex] = currentNum;
  }
  return uniqueNumArray;
}
