/**
 * This is the file we can put static data like text, numbers
 */

export default {
  RESET: 'Reset',
  NUM_LIMIT: 100,
  ARRAY_SIZE: 6,
  CONGRATS: 'Congratulations!',
  TRY_AGAIN: 'Try another round',
  STEPS: 'Steps : ',
};
