/**
 * This file can provide color constants
 */

export default {
  WHITE: '#FFFFFF',
  BLACK: '#000000',
  LIGHT_BLUE: '#3232FF',
  CYAN: '#00FFFF',
};
